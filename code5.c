/* Isuru Harischandra
 * Student ID :- 202032
 */

// This program will get an integer as an input and implements a multiplication table according to that integer

#include <stdio.h>

int main() {

    int n, iterator, row, column;

    // get user input
    printf("Enter a number :");
    scanf("%d", &n);

    // filling the table
    for (row = 1; row <= n; row++) { // swapping between rows
        for (column = 1; column <= 12; column++) { // swapping between columns
            printf("%d\t", row * column); // printing value
        }
        printf("\n\n");
    }

    return 0;

}