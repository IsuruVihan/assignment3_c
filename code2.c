/* Isuru Harischandra
 * Student ID :- 202032
 */

// This program will get an integer as input and prints whether it's a prime number or not a prime number

#include <stdio.h>

int main() {

    int number, iterate, result;

    // getting user input
    printf("Enter a number :");
    scanf("%d", &number);

    if (number == 1 || number == 0) { // if number = 0 or number = 1
        printf("It's not a prime number!");
    } else if (number == 2) { // if number = 2
        printf("It's a prime number!");
    } else {

        for (iterate = 2; iterate < number; iterate++) {
            if (number % iterate == 0) {
                result = 1;
                break;
            } else {
                result = 0;
            }
        }

        if (result == 0) {
            printf("It's a prime number!");
        } else {
            printf("It's not a prime number!");
        }

    }

    return 0;

}