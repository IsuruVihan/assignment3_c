/* Isuru Harischandra
 * Student ID :- 202032
 */

// This program will take integers as inputs and print the total of them when the user input zero or a negative integer

#include <stdio.h>

int main() {

    int number, total = 0, condition = 1;

    while(condition) {

        // getting user inputs
        printf("Enter a number :");
        scanf("%d", &number);
        
        if(number <= 0) { // if the user input is negative or zero
            break;
        }
        
        // calculating total
        total += number;

    }

    // print total
    printf("Total : %d", total);

    return 0;

}