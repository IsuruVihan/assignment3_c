/* Isuru Harischandra
 * Student ID :- 202032
 */

// This program will get an integer as an input and print all factors according to the input

#include <stdio.h>

int main() {

    int number, iterator;

    // get user input
    printf("Enter a number :");
    scanf("%d", &number);

    printf("Factors : ");
    for (iterator = 1; iterator <= number; iterator++) {
        if (number % iterator == 0) { // selecting factors
            printf("%d ", iterator); // printing factors
        }
    }

    return 0;

}