/* Isuru Harischandra
 * Student ID :- 202032
 */

// This program will get an integer as an input and print the number reversely

#include <stdio.h>

int main() {

    int number, remainder;

    // get user input
    printf("Enter a number :");
    scanf("%d", &number);

    printf("Reverse number :");
    do {
        
        remainder = number % 10; // get the remaining last digit of the number
        number /= 10; // store the new number without the last digit
        printf("%d", remainder); // printing last digit
    
    } while(number > 0);

    return 0;

}